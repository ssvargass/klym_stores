import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from '@nestjs/common';

import { ProductsModule } from './products/products.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      database: process.env.DB_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      autoLoadEntities: true,
      synchronize: true,
      cache: true,
      namingStrategy: new SnakeNamingStrategy(),
    }),
    ProductsModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
