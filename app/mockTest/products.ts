export const fixedRate = 4100;
export const mockComputedResponse = [
  {id: 'asdas', name: 'adas', price: 120000, currency: 'COP'},
  {id: 'asdas', name: 'adas', price: 20000, currency: 'COP'},
  {id: 'asdas', name: 'adas', price: 10000, currency: 'COP'},
]

export const mockDatabaseProducts = [
  {id: 'asdas', name: 'adas', price: 120000/fixedRate },
  {id: 'asdas', name: 'adas', price: 20000/fixedRate },
  {id: 'asdas', name: 'adas', price: 10000/fixedRate },
]