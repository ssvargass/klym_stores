import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable } from '@nestjs/common';
import { set } from 'lodash';

import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from "./entities/product.entity";
import axios from "axios";

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    return this.productsRepository.create(createProductDto);
  }

  async findAll(currency: string) {
    const products = await this.productsRepository.find();
    try {
      const request = await axios.get(`http://127.0.0.1/USD/to/${currency}`);
      return products.map((product) => ({
        ...product,
        price: product.price * request.rate,
        currency
      }));
    } catch (e) {
      throw new Error('Error with rate conversion');
    }
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }
}
