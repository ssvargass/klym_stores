import { Test, TestingModule } from '@nestjs/testing';
import { ProductsService } from './products.service';
import {Repository} from "typeorm";
import {Product} from "./entities/product.entity";
import {fixedRate, mockComputedResponse} from "../../mockTest/products";
import axios from "axios";

jest.mock("axios");
// @ts-ignore
export const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(() => ({
  find: jest.fn(entity => mockDatabaseProducts),
}));

describe('ProductsService', () => {
  let service: ProductsService;
  let repositoryMock: MockType<Repository<Product>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductsService],
    }).compile();

    service = module.get<ProductsService>(ProductsService);
    repositoryMock = module.get(getRepositoryToken(Product));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Called with not valid currency', async () => {
    expect(await service.findAll('ABC')).toThrow('Error with rate conversion');
    expect(await service.findAll('12312')).toThrow('Error with rate conversion');
    expect(await service.findAll('$@#$@')).toThrow('Error with rate conversion');
  })

  it('Get products with local currency', async () => {
    const respose = await service.findAll('COP')
    expect(respose.length).toEqual(4)
    expect(Object.keys(respose[1])).toEqual(4)
    expect(respose[1].currency).toEqual('COP')
  })

  it('Get computed price value', async() => {
    // Given
    const rate = {
      rate: fixedRate
    };
    axios.get.mockResolvedValueOnce(rate);

    // When
    const respose = await service.findAll('COP')

    // Then
    expect(respose).toEqual([mockComputedResponse])
  });
});
