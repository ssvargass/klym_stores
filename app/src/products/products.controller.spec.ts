import { Test, TestingModule } from '@nestjs/testing';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';

describe('ProductsController', () => {
  let controller: ProductsController;
  let service: ProductsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [ProductsService],
    }).compile();

    service = moduleRef.get<ProductsService>(ProductsService);
    controller = moduleRef.get<ProductsController>(ProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Get products with currency not supported', () => {
    expect(controller.findAll('ABC')).toThrow('Currency not supported');
    expect(controller.findAll('123')).toThrow('Currency not supported');
  })

  it('Get products with currency supported', () => {
    const result = [{id: 'asdasda', name: 'ejemplo', price: 2000}]
    // Given
    jest.spyOn(service, 'findAll').mockImplementation(() => result);

    // When
    controller.findAll('COP');

    // THEN
    expect(service).toHaveBeenCalledTimes(1);
    expect(service).toBeCalledWith('COP');
  })
});
